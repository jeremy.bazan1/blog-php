<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Entity\Article;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ArticleRepository;
use App\Form\ArticleType;

class AddArticleController extends Controller
{
    /**
     * @Route("/add/article", name="add_article")
     */
    public function index(Request $request, ArticleRepository $repo)
    {
        $form = $this->createForm(ArticleType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $repo->add($form->getData());
            return $this->redirectToRoute("article");
        }


        return $this->render('add_article/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
