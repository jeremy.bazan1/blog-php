<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Article;
use App\Form\ArticleType;


class ArticleController extends Controller
{
    /**
     * @Route("/", name="article")
     */
    public function index(ArticleRepository $repo)
    {
        $article = $repo->getAll();
        return $this->render('home/index.html.twig', [
            "article" => $article
        ]);
    }

    /**
     * @Route("/article/remove/{id}", name="remove_article")
     */
    public function remove(int $id, ArticleRepository $repo)
    {
        $repo->delete($id);
        return $this->redirectToRoute("article");
    }

}
