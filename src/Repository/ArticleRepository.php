<?php

namespace App\Repository;

use App\Entity\Article;


class ArticleRepository
{

    public function getAll() : array
    {

        //new \PDO("mysql:host=localhost:3306; dbname=mabdd", "user", "mdp");
        $articles = [];
        try {
        
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);
            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
         
            $query = $cnx->prepare("SELECT * FROM Article");
            $query->execute();

          
            foreach ($query->fetchAll() as $row) {
                $article = new Article();
                $article->fromSQL($row);
                $articles[] = $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return $articles;
    }

    public function add(Article $article)
    {
      
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("INSERT INTO Article (title, text) VALUES (:title, :text)");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":text", $article->text);

            $query->execute();

            $article->id = intval($cnx->lastInsertId());

        } catch (\PDOException $e) {
            dump($e);
        }
    }
 
    public function update(Article $article) {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("UPDATE Article SET title=:title, text=:text, 
            WHERE id=:id");
            
            $query->bindValue(":title", $article->title);
            $query->bindValue(":text", $article->text);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function delete(int $id) {
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("DELETE FROM Article WHERE id=:id");
            
            $query->bindValue(":id", $id);

            return $query->execute();

        } catch (\PDOException $e) {
            dump($e);
        }
        return false;
    }

    public function getById(int $id): ?Article{
        try {
            $cnx = new \PDO("mysql:host={$_ENV["MYSQL_HOST"]}:3306;dbname={$_ENV["MYSQL_DATABASE"]}", $_ENV["MYSQL_USER"], $_ENV["MYSQL_PASSWORD"]);

            $cnx->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $query = $cnx->prepare("SELECT * FROM Article WHERE id=:id");
            

            $query->execute();

            $result = $query->fetchAll();

            if(count($result) === 1) {
                $article = new Article();
                $article->fromSQL($result[0]);
                return $article;
            }

        } catch (\PDOException $e) {
            dump($e);
        }
        return null;
    }
}
